import {Injectable} from '@angular/core';

interface User {
  login: string;
  password?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userObject: User = null;

  constructor() {
  }

  login(login, password) {
    this.userObject = {
      login,
      password
    };
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('q');
      }, 1500);
    });
  }

  get user() {
    return this.userObject;
  }

  changeUserName(name) {
    if (!this.userObject) {
      this.userObject = {
        login: ''
      };
    }
    this.userObject.login = name;
  }
}
