import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {InfoComponent} from './components/info/info.component';
import {TabComponent} from './components/tab/tab.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', component: InfoComponent},
  {path: 'tab', component: TabComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
