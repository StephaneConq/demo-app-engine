import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  form: FormGroup;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder
  ) { }

  nameModel = '';

  ngOnInit() {
    this.form = this.fb.group({
      newName: ''
    });
  }

  getUserName() {
    return this.authService.user ? this.authService.user.login : 'pas co';
  }

  onChange(event) {
    console.log('value', this.form.getRawValue());
    this.authService.changeUserName(this.nameModel);
  }

  aRecuEvent(event) {
    console.log('event received', event);
  }


}
