import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  authFormGroup: FormGroup;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.authFormGroup = this.fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  submit() {
    this.loading = true;
    console.log('authservice', this.authService);
    this.authService.login(
      this.authFormGroup.controls['login'].value,
      this.authFormGroup.controls['password'].value).then(
      () => {
        this.loading = false;
        this.router.navigate(['']);
      });
  }

}
