import {Component, OnInit} from '@angular/core';
// @ts-ignore
import mockData from '../../../assets/mock.json';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  constructor() { }
  displayedColumns: string[] = ['columnName', 'Type', 'primaryKey', 'Mandatory', 'Delete'];
  dataSource: Array<any> = mockData;

  ngOnInit() {
    console.log('mockData', mockData);
  }

  click() {
    console.log('data', this.dataSource);
  }

  addRow() {
    const tmp = [...this.dataSource];
    console.log('tmp', tmp);
    tmp.push({
      objectGUID: 'random',
      columnName: 'pushed',
      Type: 'Integer',
      primaryKey: true,
      Mandatory: true,
      deleted: false,
      updated: false
    });
    this.dataSource = tmp;
  }

  deleteRow(event, element) {
    console.log('event', event);
    console.log('element', element);
    const tmp = [...this.dataSource];
    const idx = this.dataSource.findIndex(elt => elt.columnName === element.columnName);
    tmp.splice(idx, 1);
    this.dataSource = tmp;
  }
}
