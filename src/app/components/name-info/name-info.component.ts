import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-name-info',
  templateUrl: './name-info.component.html',
  styleUrls: ['./name-info.component.scss']
})
export class NameInfoComponent implements OnInit {

  @Input() newName;
  @Output() outputEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  click() {
    this.outputEvent.emit(this.newName);
  }

}
